class LTest extends Level{
    ship;
    test;
    buttons = [];

    WIDTH = 720;
    HEIGHT = 960;
    t = 0;
    ready = false;

    setup(){
        this.ship = GAME.GOHandler.addObjectNoTag(new GOShip(720/2,960/2,GAME.graphicsHandler.assets.sprites.objects.game.ship, GAME.graphicsHandler.SPRITE_GROUPS.OBJECTS));
        this.buttons.push(GAME.GOHandler.addObjectNoTag(new GOButton(840,154,GAME.graphicsHandler.assets.sprites.objects.keys.keyUp, () => {GAME.controls.UP.active = true;}, () => {GAME.controls.UP.active = false;})));
        this.buttons.push(GAME.GOHandler.addObjectNoTag(new GOButton(840,312,GAME.graphicsHandler.assets.sprites.objects.keys.keyLeft, () => {GAME.controls.LEFT.active = true;}, () => {GAME.controls.LEFT.active = false;})));
        this.buttons.push(GAME.GOHandler.addObjectNoTag(new GOButton(840,505,GAME.graphicsHandler.assets.sprites.objects.keys.keyDown, () => {GAME.controls.DOWN.active = true;}, () => {GAME.controls.DOWN.active = false;})));
        this.buttons.push(GAME.GOHandler.addObjectNoTag(new GOButton(840,685,GAME.graphicsHandler.assets.sprites.objects.keys.keyRight, () => {GAME.controls.RIGHT.active = true;}, () => {GAME.controls.RIGHT.active = false;})));
        this.buttons.push(GAME.GOHandler.addObjectNoTag(new GOButton(840,850,GAME.graphicsHandler.assets.sprites.objects.keys.keyShoot, () => {GAME.controls.SHOOT.active = true;}, () => {GAME.controls.SHOOT.active = false;})));
        //GAME.GOHandler.addObjectNoTag(new GOEnemyApathy(192,random(192), 300));
        this.collisionStack.push(this.ship);
        if(!TEST_MODE){
        setTimeout(() =>{
            this.buttons.forEach(item => {
                let go = GAME.getObject(item);
                AudioHandler.playShoot();
                GAME.graphicsHandler.screenShake(20,.9, () => {});
                go.changeState(go.states.float);
            });
            this.ready = true;
        },2000);
        }else{
            this.ready = true;
        }
    }

    step(){
        if(this.ready){
            if(keyDown(GAME.controls.SHOOT.key) && GAME.controls.SHOOT.active){
                this.ready = false;
                GAME.waveHandler.changeWave(0);
            }
        }
        this.t += 0.05;
    }

    drawGui(){
        noFill();
        strokeWeight(3);
        stroke(255,0,0);
        ellipse(camera.mouseX,camera.mouseY,5);
        push();
        fill(255);
        let guiWidth = GraphicsHandler.VIEWPORT_WIDTH - this.WIDTH;
        image(GAME.graphicsHandler.assets.backgrounds.ui,GraphicsHandler.VIEWPORT_WIDTH - guiWidth/2,GraphicsHandler.VIEWPORT_HEIGHT/2);
        pop();
    }

    popButton(){
        let button = GAME.getObject(random(this.buttons));
        if(button != undefined){
            button.changeState(button.states.float);
        }
    }
}
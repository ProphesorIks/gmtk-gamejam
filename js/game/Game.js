/**
 * Game class. This class is used as a main entry point for game. It inits handlers, handles game loop, levels and game states.
 */
//load 
//class
class Game{
    graphicsHandler;
    audioHandler;
    levelHandler
    GOHandler;
    waveHandler;
    timeSpeed = 1;

    controls = {
        UP:{ 
            key: 'w',
            active: true
        },
        DOWN: {
            key: 's',
            active: true
        },
        LEFT:{
            key: 'a',
            active: true
        },
        RIGHT: {
            key: 'd',
            active: true
        },
        SHOOT: {
            key: ' ',
            active: true
        }
    }
    
    preload(){
        this.graphicsHandler = new GraphicsHandler();
        this.audioHandler = new AudioHandler();
        this.GOHandler = new GameObjectHandler();
        this.levelHandler = new LevelHandler(new Level());
        this.waveHandler = new WaveHandler();

        this.graphicsHandler.preload();
        this.audioHandler.preload();
    }

    setup(){
        createCanvas(GraphicsHandler.VIEWPORT_WIDTH, GraphicsHandler.VIEWPORT_HEIGHT);
        this.levelHandler.changeLevel(new LTest());
        this.graphicsHandler.setup();
        angleMode(DEGREES);
        this.graphicsHandler.transitionFade(.01,255,() => {});
        focused = true;
        //FIX me: test music
    }

    draw(){
        this.graphicsHandler.draw();
        if(!focused || GAME.PAUSE){
            this.graphicsHandler.drawPause();
        }
    }

    step(){
        if(focused && !GAME.PAUSE){
            this.levelHandler.step();
            this.GOHandler.step();
        }
        AudioHandler.step();
    }

    getObject(tag){
        return this.GOHandler.getObject(tag);
    }
}

var GAME;
Game.FPS = 60;
Game.PAUSE = false;
Game.player = "";
var dt = () => {return deltaTime/Game.FPS}
const TEST_MODE = false;


preload = () => {
    GAME = new Game();
    GAME.preload();
}

setup = () => {
    GAME.setup();
};
  
draw = () => {
    GAME.draw();
    GAME.step(deltaTime / 50);
};
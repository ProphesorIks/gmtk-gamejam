class GameObjectHandler {
    objectStack = new Map();

    constructor() {

    }

    step(){
        this.objectStack.forEach(item => {
            if(item != undefined){
                if(!item.ended){
                    item.step();
                }
                if(TEST_MODE){
                    item.drawBoundingBox();
                }
            }else{
                console.log("turbo-log: GameObjectHandler -> step -> "," Tried to call step for undefined GO");
            }
        });
    }

    addObject(tag, gameObject){
        this.objectStack.set(tag, gameObject);
        gameObject.tag = tag;
    }

    addObjectNoTag(gameObject){
        let uuid = Utils.createUUID();
        this.addObject(uuid,gameObject);
        return uuid;
    }

    removeObject(tag){
        let o = this.objectStack.get(tag);
        if(o != undefined){
            o.delete();
        }
        
        // if(!this.objectStack.delete(tag)){
        //     console.log("tried to remove non present GO: " + tag);
        // }
    }

    getObject(tag){
        return this.objectStack.get(tag);
    }

    clear(){
        this.objectStack.forEach(item => item.delete());
        this.objectStack.clear();
    }

}
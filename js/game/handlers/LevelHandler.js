class LevelHandler{
    static levels ={
        menu: 0,
    }

    WORLD_WIDTH = 1280;
    WORLD_HEIGHT = 720;

    objectHandler;
    currentLevel;

    constructor(level){
        this.changeLevel(level);
    }

    changeLevel(level){
        if(this.currentLevel != undefined){
            this.currentLevel.end();
        }
        this.currentLevel = level;
        this.WORLD_WIDTH = level.WIDTH;
        this.WORLD_HEIGHT = level.HEIGHT;
        this.currentLevel.setup();
    }

    step(){
        this.currentLevel.step();
    }
}
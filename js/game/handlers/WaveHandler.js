class WaveHandler {
    waveCount = 6;
    currentWave = 0;
    enemyStack = [];
    waveCooldown = 5000;
    
    changeWave(wave){
        switch(wave){
            case 0: {
                GAME.graphicsHandler.background.images.level = 0;
                this.addEnemy(GAME.GOHandler.addObjectNoTag(new GOEnemyRage(192,random(192), -300)));
                this.addEnemy(GAME.GOHandler.addObjectNoTag(new GOEnemyRage(576,random(192), -300)));
                AudioHandler.playMusic(AudioHandler.sounds.music.tunes[0]);
                break;
            }
            case 1: {
                GAME.levelHandler.currentLevel.popButton();
                this.addEnemy(GAME.GOHandler.addObjectNoTag(new GOEnemyRage(192,random(192), -300)));
                this.addEnemy(GAME.GOHandler.addObjectNoTag(new GOEnemyRage(576,random(192), -300)));
                this.addEnemy(GAME.GOHandler.addObjectNoTag(new GOEnemyRage(192,random(192), -800)));
                this.addEnemy(GAME.GOHandler.addObjectNoTag(new GOEnemyApathy(576,random(192), -800)));
                break;
            }
            case 2: {
                setTimeout(() => {GAME.levelHandler.currentLevel.popButton();}, random(5000,10000));
                AudioHandler.playMusic(AudioHandler.sounds.music.tunes[2]);
                GAME.graphicsHandler.background.images.level = 1;
                AudioHandler.playMusic(AudioHandler.sounds.music.tunes[1]);
                this.addEnemy(GAME.GOHandler.addObjectNoTag(new GOEnemyRage(192,random(192), -300)));
                this.addEnemy(GAME.GOHandler.addObjectNoTag(new GOEnemyRage(576,random(192), -300)));
                this.addEnemy(GAME.GOHandler.addObjectNoTag(new GOEnemyApathy(576,random(192), -800)));
                this.addEnemy(GAME.GOHandler.addObjectNoTag(new GOEnemyApathy(192,random(192), -800)));
                break;
            }
            case 3: {
                GAME.levelHandler.currentLevel.popButton();
                GAME.levelHandler.currentLevel.popButton();
                AudioHandler.playMusic(AudioHandler.sounds.music.tunes[3]);
                break;
            }
            case 4: {
                break;
            }
            case 5: {
                GAME.graphicsHandler.background.images.level = 2;
                AudioHandler.playMusic(AudioHandler.sounds.music.tunes[4]);
                break;
            }
        }
        GAME.graphicsHandler.transitionFade(.05,255, () => {this.currentWave = wave;});
    }

    step(){
        
    }

    addEnemy(enemy){
        this.enemyStack.push(enemy);
        GAME.levelHandler.currentLevel.collisionStack.push(enemy);
        
    }

    enemyDestroyed(tag){
        if(this.enemyStack.includes(tag)){
            this.enemyStack = Utils.arrayRemove(this.enemyStack,tag);
            if(this.enemyStack.length <= 0){
                if(this.currentWave < this.waveCount){
                    setTimeout(() => {this.changeWave(++this.currentWave);}, this.waveCooldown);
                }else{
                    //TODO: implement ending
                }
            }
        }
    }
}
class AudioHandler{
    static sfx = new Map();
    static music = new Map();
    static baseMasterVolume = .95;
    static isSoundPlaying = true;

    static sounds = {
        sfx: {
            game: {
                ship: {
                    shoot: Utils.createUUID()
                },
                death: [

                ],
                hammer: [

                ],
                shoot: [

                ],
                bossDeath: Utils.createUUID()
            }
        },
        music: {
            tunes: [
                Utils.createUUID(),
                Utils.createUUID(),
                Utils.createUUID(),
                Utils.createUUID(),
                Utils.createUUID()
            ],
            ending: Utils.createUUID(),
            blend: Utils.createUUID()
            
        }
    }

    static musicCurrent = null;
    static musicToBePlayed = null;

    constructor(){
        userStartAudio();
        setInterval(() => {AudioHandler.updateMusic()},2264.2);
        masterVolume(AudioHandler.baseMasterVolume);
    }

    static step(){
        if(GAME.PAUSED || !focused){
            if(AudioHandler.isSoundPlaying){
                masterVolume(0,2);
                AudioHandler.isSoundPlaying = false;
            }
        }else{
            if(!AudioHandler.isSoundPlaying){
                masterVolume(AudioHandler.baseMasterVolume,2);
                AudioHandler.isSoundPlaying = true;
            }
        }
    }

    preload(){
        const FORMAT = ".ogg";
        const PATH = "assets/audio/";
        const PATH_SFX = PATH + "sfx/";
        const PATH_MUSIC = PATH + "music/";

        soundFormats('mp3', 'ogg');
        for(let i = 0; i < 5; i++){
            AudioHandler.addMusic(AudioHandler.sounds.music.tunes[i], loadSound(PATH_MUSIC + i + FORMAT));
        }

        AudioHandler.addMusic(AudioHandler.sounds.music.ending, loadSound(PATH_MUSIC + "ending" + FORMAT));
        AudioHandler.addMusic(AudioHandler.sounds.music.blend, loadSound(PATH_MUSIC + "blend" + FORMAT));
        for(let i = 0; i < 8; i++){
            AudioHandler.addSfx("shoot"+i,loadSound(PATH_SFX + "shoot" + i + FORMAT));
        }
        for(let i = 0; i < 3; i++){
            AudioHandler.addSfx("hammer"+i,loadSound(PATH_SFX + "hammer" + i + FORMAT));
        }
        for(let i = 0; i < 8; i++){
            AudioHandler.addSfx("death"+i,loadSound(PATH_SFX + "death" + i + FORMAT));
        }
        AudioHandler.addSfx(AudioHandler.sounds.sfx.game.bossDeath,loadSound(PATH_SFX + "boss_death" + FORMAT));
    }


    static playShoot(){
        AudioHandler.getSfx("shoot" + floor(random(7))).play();
    }
    static playDeath(){
        AudioHandler.getSfx("death" +floor(random(7))).play();
    }
    static playHammer(which){
        let snd = AudioHandler.getSfx("hammer" +which);
        let prev = AudioHandler.getSfx("hammer" +(which - 1));
        if(snd != undefined){
            if(prev != undefined){
                prev.stop();
            }
            snd.play();
        }
    }

    static updateMusic(){
        let current = AudioHandler.getMusic(AudioHandler.musicCurrent);
        let toBePlayed = AudioHandler.getMusic(AudioHandler.musicToBePlayed);

        if(current != toBePlayed){
            if(AudioHandler.musicCurrent != null && AudioHandler.musicToBePlayed != null){
                current.setVolume(0,2);
            }
            if(toBePlayed != null){
                toBePlayed.loop();
                toBePlayed.stop();
                toBePlayed.play();
                toBePlayed.setVolume(0);
                toBePlayed.setVolume(1,2);
                AudioHandler.musicCurrent = AudioHandler.musicToBePlayed;
                AudioHandler.musicToBePlayed = null;
            }
        }
    }

    static playMusic(name){
        AudioHandler.musicToBePlayed = name;
    }

    static pauseSfx(){
        this.sfx.forEach(element => {
            if(element != undefined){
                element.pause();
            }
        });
    }

    static unpauseSfx(){
        this.sfx.forEach(element => {
            if(element != undefined){
                element.play();
            }
        });
    }

    static stopSfx(){
        this.sfx.forEach(element => {
            if(element != undefined){
                element.stop();
            }
        });
    }

    static pauseMusic(){
        this.music.forEach(element => {
            if(element != undefined){
                element.pause();
            }
        });
    }

    static unpauseMusic(){
        this.music.forEach(element => {
            if(element != undefined){
                element.play();
            }
        });
    }

    static stopMusic(){
        this.music.forEach(element => {
            if(element != undefined){
                element.stop();
            }
        });
    }


    static addSfx(tag, sound){
        AudioHandler.sfx.set(tag, sound);
    }

    static getSfx(tag){
        return AudioHandler.sfx.get(tag);
    }

    static deleteSfx(tag){
        return AudioHandler.sfx.delete(tag);
    }

    static addMusic(tag, sound){
        AudioHandler.music.set(tag, sound);
    }

    static getMusic(tag){
        return AudioHandler.music.get(tag);
    }

    static deleteMusic(tag){
        return AudioHandler.music.delete(tag);
    }
}
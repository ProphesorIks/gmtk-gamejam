/**
 * This class is used for managing sprites and graphics
 */
class GraphicsHandler{
    static VIEWPORT_WIDTH = 960;
    static VIEWPORT_HEIGHT = 960;
    viewport = {
        x: 0,
        y: 0
    }

    transition = {
        types: {
            fade: 0
        },
        type: {
            fade: {
                color: 0
            }
        },
        color: 0,
        callback: () => {},
        speed: 0,
        completeness: 0
    };

    SPRITE_GROUPS = {
        BACKGROUND: new Group(),
        OBJECTS: new Group(),
        GUI: new Group()
    };

    assets = {
        backgrounds: {
            ui: new p5.Image(),
            game: [
                [],
                [],
                []
            ]
        },
        sprites: {
            objects:{
                game:{
                    ship: new Animation(),
                    bullet: new p5.Image(),
                    enemy: {
                        rage: new Animation(),
                        despair: new Animation(),
                        anxiety: new Animation(),
                        depression: new Animation(),
                        flower:{
                            open: new Animation(),
                            close: new Animation(),
                            glow: new Animation(),
                            bloom: new Animation()
                        },
                        boss: {
                            head: new Animation(),
                            teeth: new Animation()
                        }
                    }
                },
                keys: {
                    keyUp: new p5.Image(),
                    keyDown: new p5.Image(),
                    keyLeft: new p5.Image(),
                    keyRight: new p5.Image(),
                    keyShoot: new p5.Image()
                }
            },
            hammer: new p5.Image()
            
        }
    };

    background = {
        speed: [
            3,
            1,
            6,
        ],
        height: 2528,
        width: 720,
        images: {
            position: [0,0,0,0],
            level: 1
        }
    }

    virtualCam = {
        xTo: camera.position.x,
        yTo: camera.position.y,
        ease: 25,
        shake: 0,
        shakeFalloff: .9
    };

    setup(){
        imageMode(CENTER);
        noCursor();
        frameRate(GAME.FPS);
    }

    preload(){
        const FORMAT = ".png";
        const PATH = "assets/graphics/";
        const PATH_BACKGROUNDS = PATH + "background/";
        const PATH_SPRITES = PATH + "sprite/";
        
        //UI
        this.assets.backgrounds.ui = loadImage(PATH_BACKGROUNDS + "ui" + FORMAT);
        this.assets.sprites.objects.keys.keyUp = loadImage(PATH_SPRITES + "keys/up0" + FORMAT);
        this.assets.sprites.objects.keys.keyDown = loadImage(PATH_SPRITES + "keys/down0" + FORMAT);
        this.assets.sprites.objects.keys.keyLeft = loadImage(PATH_SPRITES + "keys/left0" + FORMAT);
        this.assets.sprites.objects.keys.keyRight = loadImage(PATH_SPRITES + "keys/right0" + FORMAT);
        this.assets.sprites.objects.keys.keyShoot = loadImage(PATH_SPRITES + "keys/shoot0" + FORMAT);
        this.assets.sprites.hammer = loadImage(PATH_SPRITES + "hammer" + FORMAT);

        //backgrounds
        for(let i = 0; i < 3; i++){
            for(let j = 0; j < 4; j++){
                this.assets.backgrounds.game[i][j] = loadImage(PATH_BACKGROUNDS + i +"/"+ j + FORMAT);
            }
            
        }

        //special
        this.assets.sprites.objects.game.ship = loadAnimation(PATH_SPRITES + "player/0" + FORMAT, PATH_SPRITES + "player/9" + FORMAT);
        this.assets.sprites.objects.game.bullet = loadImage(PATH_SPRITES + "bullet" + FORMAT);

        //load enemies
        let quickLoad = (path,length) => {
            return loadAnimation(PATH_SPRITES + path + "0" + FORMAT, PATH_SPRITES + path + length + FORMAT);
        }
        this.assets.sprites.objects.game.enemy.rage = quickLoad("enemy/rage/","9");
        this.assets.sprites.objects.game.enemy.anxiety = quickLoad("enemy/anxiety/","11");
        this.assets.sprites.objects.game.enemy.depression = quickLoad("enemy/depression/","22");
        this.assets.sprites.objects.game.enemy.despair = quickLoad("enemy/despair/","25");
        this.assets.sprites.objects.game.enemy.boss.head = quickLoad("enemy/boss/head/","5");
        this.assets.sprites.objects.game.enemy.boss.teeth = quickLoad("enemy/boss/teeth/","3");
        this.assets.sprites.objects.game.enemy.flower.open = quickLoad("enemy/flower/open/","5");
        this.assets.sprites.objects.game.enemy.flower.glow = quickLoad("enemy/flower/glow/","9");
        this.assets.sprites.objects.game.enemy.flower.close = quickLoad("enemy/flower/close/","9");
        this.assets.sprites.objects.game.enemy.flower.bloom = quickLoad("enemy/flower/bloom/","23");
    }

    draw(){
        this.transitionStep();
        this.updateCamera();
        this.updateViewPort();
        background(10);
        this.drawBackground();
        camera.on();
        drawSprites(this.SPRITE_GROUPS.BACKGROUND);
        drawSprites(this.SPRITE_GROUPS.OBJECTS);
        camera.off();
        this.drawGui();
    };

    drawGui(){
        push();
        translate(this.viewport.x,this.viewport.y);
        if(GAME.levelHandler.currentLevel != undefined){
            GAME.levelHandler.currentLevel.drawGui();
        }
        drawSprites(this.SPRITE_GROUPS.GUI);
        this.transitionDraw();

        if(TEST_MODE){
            noStroke();
            fill(0,255,0);
            text(floor(getFrameRate()),10,10);
        }
        if(mouseDown(LEFT)){
            rotate(1);
        }
        image(this.assets.sprites.hammer,camera.mouseX,camera.mouseY);
        pop();
    }

    drawBackground(){
        push();
        for(let i = 0; i < 4; i++){
            //first image
            image(
                this.assets.backgrounds.game[this.background.images.level][i],
                this.background.width/2,
                this.background.images.position[i] + this.background.height/2,
                this.background.width,
                this.background.height
            );
            //second image
            image(
                this.assets.backgrounds.game[this.background.images.level][i],
                this.background.width/2,
                this.background.images.position[i] - this.background.height/2,
                this.background.width,
                this.background.height
            );
            
            this.background.images.position[i] += this.background.speed[i];
            this.background.images.position[i] %= this.background.height;
        }
        pop();
    }

    drawPause(){
        push();
        background(0,220);
        textFont('Courier New');
        textStyle(ITALIC);
        textAlign(CENTER, CENTER);
        textSize(40);
        fill(255);
        noStroke();
        text("Game is paused, click to unpause",width/2,height/2);
        pop();
    }

    pointCamera(x, y){
        virtualCam.xTo = x;
        virtualCam.yYo = y;
    };

    //eases the camera and updates in game camera to follow the virtual camera
    updateCamera(){
        camera.position.x += ((this.virtualCam.xTo - camera.position.x) / this.virtualCam.ease) * dt();
        camera.position.y += ((this.virtualCam.yTo - camera.position.y) / this.virtualCam.ease) * dt();

        //don't let camera go outside of the world
        camera.position.x = constrain(
            camera.position.x,
            GraphicsHandler.VIEWPORT_WIDTH / 2,
            GAME.levelHandler.WORLD_WIDTH - GraphicsHandler.VIEWPORT_WIDTH / 2
        );
        camera.position.y = constrain(
            camera.position.y,
            GraphicsHandler.VIEWPORT_HEIGHT / 2,
            GAME.levelHandler.WORLD_WIDTH  - GraphicsHandler.VIEWPORT_HEIGHT / 2
        );

        //screen shake
        camera.position.x += random(-this.virtualCam.shake, this.virtualCam.shake);
        camera.position.y += random(-this.virtualCam.shake, this.virtualCam.shake);
        this.virtualCam.shake *= this.virtualCam.shakeFalloff;
    };

    updateViewPort(){
        this.viewport.x += random(-this.virtualCam.shake, this.virtualCam.shake);
        this.viewport.y += random(-this.virtualCam.shake, this.virtualCam.shake);
        this.viewport.x = Utils.approach(this.viewport.x, 0, .1 + dt()*this.virtualCam.shake);
        this.viewport.y = Utils.approach(this.viewport.y, 0, .1 + dt()*this.virtualCam.shake);
    }

    screenShake(strength, falloff){
        this.virtualCam.shake = max(this.virtualCam.shake, strength);
        this.virtualCam.shakeFalloff = falloff;
    };

    transitionFade(speed,color,callBack){
        this.transition.completeness = 1;
        this.transition.speed = speed;
        this.transition.color = color;
        this.transition.callback = callBack;
    };

    transitionStep(){
        if(this.transition.completeness > 0){
            this.transition.completeness = Utils.approach(this.transition.completeness, 0, abs(this.transition.speed));
            if(this.transition.completeness <= 0){
                this.transition.callback();
            }
        }
    }

    transitionDraw(){
        if(this.transition.completeness != 0){
            let a = this.transition.completeness;
            if(this.transition.speed < 0){
                a = 1 - a;
            }
            noStroke();
            fill("rgba("+this.transition.color +","+this.transition.color +","+this.transition.color +", " + a + ")");
            rect(0,0,width,height);
        }
    }
}
class GameObject {
    x = 0;
    y = 0;
    sprite;
    width = 1;
    height = 1;
    tag = "";
    ended = false;
    state;
    states = {};

    constructor(x, y, spriteImage, spriteGroup) {
        this.x = x;
        this.y = y;
        if(spriteImage != undefined){
            this.sprite = createSprite(this.x, this.y, this.width, this.height);
            this.sprite.depth = 0;
            this.sprite.addImage(spriteImage);
            if(spriteGroup != undefined){
                spriteGroup.add(this.sprite);
            }
        }
    }

    step(){ 
        this.updateSprite(); 
    };

    changeState(state){
        this.state = state;
    }

    updateSprite() {
        if(this.sprite != undefined){
            this.sprite.position.x = this.x;
            this.sprite.position.y = this.y;
            this.sprite.update();
        }
    }

    //prevents object from goin offscreen
    constrainToWorld() {
        this.x = constrain(this.x, this.width/2, GAME.levelHandler.WORLD_WIDTH - this.width/2);
        this.y = constrain(this.y, this.height/2, GAME.levelHandler.WORLD_HEIGHT - this.height/2);
    }

    drawBoundingBox(){
        stroke(0,0,255);
        noFill();
        strokeWeight(2);
        rect(this.x - this.width/2, this.y - this.height/2, this.width, this.height);
    }

    checkCollide(other){
        if(other.x - other.width/2 < this.x + this.width/2
             && other.x + other.width/2 > this.x - this.width/2){
                if(other.y - other.height/2 < this.y + this.height/2
                    && other.y + other.height/2 > this.y - this.height/2){
                        return true;
                }
        }
        return false;
    }

    collide(other,callback){
        if(callback != undefined){
            callback();
        }
    }


    end(){
        this.ended = true;
        GAME.GOHandler.removeObject(this.tag);
        Utils.arrayRemove(GAME.levelHandler.currentLevel.collisionStack,this.tag);
    }
    //clean up
    delete(){
        if(this.sprite != null){
            this.sprite.remove();
        }
        this.sprite = null;
    }
}

/**
 * Various utils
 */
class Utils{}
Utils.approach = (value, target, amount) => {
    if(value < target){
        value += amount;
        if(value > target){
            return target;
        }
    }else{
        value -= amount;
        if(value < target){
            return target;
        }
    }
    return value;
}

Utils.createUUID = () => {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    )
}

Utils.distance = (x,x1) => {
    return Math.abs(x - x1)
}

Utils.inRange = (value,target,range) => {
    return (Utils.distance(value,target) <= range);
}

Utils.arrayRemove = (arr, value) => {
    return arr.filter((ele) => { 
        return ele != value; 
    });
}
class GOShip extends GameObject{

    dir = 0;
    shootCooldown = 200; //how long to delay between shots
    canShoot = true;
    speed = {
        max: 25,
        acceleration: 5,
        friction: .2,
        x: 0,
        y: 0
    }

    states = {
        paused: Utils.createUUID(),
        normal: Utils.createUUID(),
        invincible: Utils.createUUID()
    }

    state = this.states.paused;
    invincibilityTime = 1000;
    healthMax = 20;
    health = 10;
    

    constructor(x,y,image, spriteGroup){
        super(x,y,null);
        this.sprite = createSprite(this.x, this.y, this.width, this.height);
        this.sprite.addAnimation("main",GAME.graphicsHandler.assets.sprites.objects.game.ship);
        GAME.graphicsHandler.SPRITE_GROUPS.OBJECTS.add(this.sprite);
        this.width = 32;
        this.height = 32;
        this.sprite.depth = 10; 
        this.changeState(this.states.normal);
        
    }

    step(){
        GAME.player = this.tag;
        switch(this.state){
            case (this.states.normal): {
                this.control();
                //shoot
                if(keyDown(GAME.controls.SHOOT.key) && GAME.controls.SHOOT.active){
                    this.shoot();
                }
                break;
            }

            case (this.states.invincible): {
                this.control();
                this.sprite.visible = !this.sprite.visible;
                break;
            }
            
            default: {
                console.log(this.state);
            }

        }
        //controls
        this.move();

        //animate
        this.animate();
        
        this.constrainToWorld();
        this.updateSprite();

        if(this.health <= 0){
            this.end();
        }
    }

    collide(other,callback){
        if(other.owner != this.tag && this.state != this.states.invincible){
            this.impact();
            super.collide(other,callback);
        }
    }

    shoot(){
        if(this.canShoot && !this.ended){
            GAME.GOHandler.addObjectNoTag(new GOBullet(this.x,this.y,-40, this.tag));
            GAME.graphicsHandler.screenShake(5,.9);
            AudioHandler.playShoot();
            this.canShoot = false;
            setTimeout(() => {this.canShoot = true}, this.shootCooldown);
        }
    }

    changeState(state){
        this.state = state;
        if(state == this.states.normal){
            this.sprite.visible = true;
        }

    }

    impact(){
        this.health--;
        this.changeState(this.states.invincible);
        setTimeout(() => {this.changeState(this.states.normal);}, this.invincibilityTime);
    }

    move(){
        this.x += this.speed.x * dt() * GAME.timeSpeed;
        this.y += this.speed.y * dt() * GAME.timeSpeed;
    }

    control(){
        let controlY = (keyDown(GAME.controls.DOWN.key) && GAME.controls.DOWN.active) - (keyDown(GAME.controls.UP.key) && GAME.controls.UP.active);
        let controlX = (keyDown(GAME.controls.RIGHT.key) && GAME.controls.RIGHT.active) - (keyDown(GAME.controls.LEFT.key) && GAME.controls.LEFT.active);

        this.speed.x = Utils.approach(this.speed.x, this.speed.max * controlX , this.speed.acceleration);
        this.speed.y = Utils.approach(this.speed.y, this.speed.max * controlY, this.speed.acceleration);
    }

    animate(){
        if(this.sprite != null)
            this.sprite.rotation = this.dir;

        fill(255,0,0,100);
        noStroke();
        let wd = (this.health*720)/this.healthMax;
        rect(0,960-16,wd,16);
        fill(25,52,0,100);
        rect(0,0,(GAME.waveHandler.currentWave*720)/GAME.waveHandler.waveCount,16);
    }

}
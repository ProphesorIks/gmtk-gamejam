class GOBullet extends GameObject{
    speed = 10;
    dir = -90;
    owner = "";

    constructor(x,y,speed,owner){
        super(x,y,GAME.graphicsHandler.assets.sprites.objects.game.bullet, GAME.graphicsHandler.SPRITE_GROUPS.OBJECTS);
        this.width = 10;
        this.height = 10;
        this.speed = speed;
        this.owner = owner;
    }
    
    step(){
        //this.x += this.speed * dt() * GAME.timeSpeed;
        this.y += this.speed * dt() * GAME.timeSpeed;
        this.sprite.rotation = this.dir;
        this.updateSprite();
        this.outsideRoom();
        
        GAME.levelHandler.currentLevel.collisionStack.forEach(element => {
            let other = GAME.getObject(element);
            if(other != undefined && other.tag != this.owner){
                if(other.checkCollide(this)){
                    other.collide(this,() => {
                        this.end();
                    });
                }
            }
        });
    }

    outsideRoom(){
        if(this.x < 0 -this.width
             || this.x > GAME.levelHandler.WORLD_WIDTH + this.width
             || this.y < -this.height
             || this.y > GAME.levelHandler.WORLD_HEIGHT + this.height){
                 this.end();
        }
    }
}
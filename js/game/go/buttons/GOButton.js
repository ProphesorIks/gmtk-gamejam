class GOButton extends GameObject{
    originX;
    originY;
    snapRange = 16;

    states = {
        normal: Utils.createUUID(),
        float: Utils.createUUID(),
        snapped: Utils.createUUID()
    }

    state = this.states;
    hammersMax = 3;
    hammers = this.hammersMax;

    speed = {
        x: 0,
        y: 0,
        maxSpeed: 5
    }

    onSnap;
    onFloat;

    constructor(x,y,image, onSnap, onFloat){
        super(x,y,image,GAME.graphicsHandler.SPRITE_GROUPS.GUI);
        this.originX = x;
        this.originY = y;
        this.onSnap = onSnap;
        this.onFloat = onFloat;
        this.width = 40;
        this.height = 40;
    }

    step(){
        switch(this.state){
            case(this.states.float):{
                this.move();
                if(mouseDown(LEFT)){
                    if(Utils.inRange(camera.mouseX,this.x,this.width) && Utils.inRange(camera.mouseY,this.y,this.height)){
                        this.x = mouseX;
                        this.y = mouseY;
    
                        if(Utils.inRange(this.x,this.originX,this.snapRange) && Utils.inRange(this.y,this.originY,this.snapRange)){
                            this.x = this.originX;
                            this.y = this.originY;
                            this.changeState(this.states.snapped);
                        }
                    }
                }

                break;
            }
            case(this.states.snapped):{
                this.sprite.visible = !this.sprite.visible;
                if(mouseWentDown(LEFT)){
                    if(Utils.inRange(camera.mouseX,this.x,this.width) && Utils.inRange(camera.mouseY,this.y,this.height)){
                    if(this.hammers < this.hammersMax){
                        AudioHandler.playHammer(this.hammers);
                        GAME.graphicsHandler.screenShake(10,.9, () => {});
                        this.hammers++;
                        if(this.hammers >= this.hammersMax){
                            this.changeState(this.states.normal);
                        }
                    }
                    }
                }
                break;
                
            }
        }
        this.updateSprite();
    }

    changeState(state){
        super.changeState(state);
        this.sprite.visible = true;
        if(state == this.states.float){
            this.hammers = 0;
            this.onFloat();
            this.speed.x = random(-this.speed.maxSpeed,this.speed.maxSpeed);
            this.speed.y = random(-this.speed.maxSpeed,this.speed.maxSpeed);
        }
        if(state == this.states.normal){
            this.onSnap();
        }
    }

    move(){
        if(this.x + this.speed.x - this.width/2 < 0 || this.x + this.speed.x + this.width/2 > GraphicsHandler.VIEWPORT_WIDTH){
            this.speed.x *= -1;
        }
        if(this.y + this.speed.y - this.height/2 < 0 || this.y + this.speed.y + this.height/2 > GraphicsHandler.VIEWPORT_HEIGHT){
            this.speed.y *= -1;
        }
        //move and bounce around!
        this.x += this.speed.x;
        this.y += this.speed.y;
        this.x = constrain(this.x, this.width/2, GraphicsHandler.VIEWPORT_WIDTH - this.width/2);
        this.y = constrain(this.y, this.height/2, GraphicsHandler.VIEWPORT_HEIGHT - this.height/2);
    }


}
class GOEnemy extends GameObject{
    health = 10;
    speed = 0;
    dir = 0;

    states = {
        start: Utils.createUUID(),
        normal: Utils.createUUID(),
        wonder: Utils.createUUID()
    }
    
    state = this.states.start;

    changeState(state){ this.state = state}

    end() {
        AudioHandler.playDeath();
        super.end();
        GAME.waveHandler.enemyDestroyed(this.tag);
    }

    step(){
        super.step();
        this.sprite.rotation = this.dir;
    }

    collide(other, callback){
        if(other.owner != undefined){
            if(other.owner == GAME.player){
                this.health--;
                super.collide(other, callback);
            }
        }
    }
    
}
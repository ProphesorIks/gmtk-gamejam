class GOEnemyRage extends GOEnemy{
    target = {
        x: this.x,
        y: this.y,
        smoothness: 5,
        wonder: {
            interval: 4000,
            variation: 2000,
            x: 10,
            y: 10,
            width: 192,
            height: 192
        },
        range: 32
    }

    shootCooldown = 500;

    shotsMax = 3;
    shots = 0;

    speed = 20;
    
    constructor(x,y,startY){
        super(x,startY,null);
        this.sprite = createSprite(this.x, this.y, this.width, this.height);
        this.sprite.addAnimation("main",GAME.graphicsHandler.assets.sprites.objects.game.enemy.rage);
        GAME.graphicsHandler.SPRITE_GROUPS.OBJECTS.add(this.sprite);
        
        this.sprite.depth = 0;
        this.width = 96;
        this.height = 96;
        this.health = 7;
        this.target.range = this.width/2;
        this.target.x = this.x;
        this.target.y = y;
        this.target.wonder.x = x;
        this.target.wonder.y = y;
    }

    step(){
        super.step();
        if(this.health <= 0){
            this.end();
        }

        switch(this.state){
             case this.states.wonder :{
                 this.x += ((this.target.x - this.x) / this.target.smoothness) * dt() * GAME.timeSpeed;
                 this.y += ((this.target.y - this.y) / this.target.smoothness) * dt() * GAME.timeSpeed;
                 
                 if(Utils.inRange(this.target.x, this.x, this.target.range) && Utils.inRange(this.target.y, this.y, this.target.range)){
                    this.changeState(this.states.normal);
                 }
                 
                 this.constrainToWorld();
                 break;
             }
             
             case this.states.start: {
                this.x += ((this.target.x - this.x) / this.target.smoothness) * dt() * GAME.timeSpeed;
                this.y += ((this.target.y - this.y) / this.target.smoothness) * dt() * GAME.timeSpeed;
                if(Utils.inRange(this.target.x, this.x, this.target.range) && Utils.inRange(this.target.y, this.y, this.target.range)){
                    this.wonder();
                }
                break;
             }
        }

        fill(0,255,0);
        text(this.health,this.x,this.y-this.width);
    }

    end(){
        super.end();
    }

    changeState(state){
        if(state != this.state){
            super.changeState(state);
            if(state == this.states.normal){
                this.shots = this.shotsMax;
                this.shoot();
            }
        }
    }

    shoot(){
        if(this.state == this.states.normal && !this.ended && this.shots > 0){
            GAME.GOHandler.addObjectNoTag(new GOBullet(this.x,this.y,40, this.tag));
            this.shots--;
            GAME.graphicsHandler.screenShake(2,.9);
            AudioHandler.playShoot();
            setTimeout(() => {this.shoot()},this.shootCooldown);
        }
    }

    wonder(){
        let time = this.target.wonder.interval + random(this.target.wonder.variation);
        let ship = GAME.getObject(GAME.getObject(GAME.player));
        if(ship != undefined){
            this.target.x = constrain(ship.x,this.target.wonder.x - this.target.wonder.width/2,this.target.wonder.x + this.target.wonder.width/2);//random(padding,GAME.levelHandler.WORLD_WIDTH-padding);
            this.target.y = random(this.target.wonder.y - this.target.wonder.height/2, this.target.wonder.y + this.target.wonder.height/2);
        }

        this.changeState(this.states.wonder);
        setTimeout(() => {this.wonder()},time);
    }
}
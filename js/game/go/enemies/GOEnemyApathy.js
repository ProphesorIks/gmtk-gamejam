class GOEnemyApathy extends GOEnemy{
    target = {
        x: this.x,
        y: this.y,
        smoothness: 10,
        wonder: {
            interval: 6000,
            variation: 4000,
            x: 350,
            y: 450,
            width: 700,
            height: 900
        },
        range: 32
    }

    range = 48;
    
    constructor(x,y,startY){
        super(x,startY,null);
        this.sprite = createSprite(this.x, this.y, this.width, this.height);
        this.sprite.addAnimation("main",GAME.graphicsHandler.assets.sprites.objects.game.enemy.anxiety);
        GAME.graphicsHandler.SPRITE_GROUPS.OBJECTS.add(this.sprite);
        
        this.sprite.depth = 0;
        this.width = 64;
        this.height = 64;
        this.health = 7;
        this.target.range = this.width/2;
        this.target.x = this.x;
        this.target.y = y;
        setInterval(() => {this.canDamage = true}, 1500); //damage time

    }

    step(){
        super.step();
        if(this.health <= 0){
            this.end();
        }

        switch(this.state){
             case this.states.wonder :{
                 this.x += ((this.target.x - this.x) / this.target.smoothness) * dt() * GAME.timeSpeed;
                 this.y += ((this.target.y - this.y) / this.target.smoothness) * dt() * GAME.timeSpeed;
                 
                 this.constrainToWorld();
                 break;
             }
             
             case this.states.start: {
                this.x += ((this.target.x - this.x) / this.target.smoothness) * dt() * GAME.timeSpeed;
                this.y += ((this.target.y - this.y) / this.target.smoothness) * dt() * GAME.timeSpeed;
                if(Utils.inRange(this.target.x, this.x, this.target.range) && Utils.inRange(this.target.y, this.y, this.target.range)){
                    this.wonder();
                }
                break;
             }


        }
        if(this.getDistance() <= this.range && this.canDamage){
            GAME.getObject(GAME.player).impact();
            this.canDamage = false;
        }
        fill(0,255,0);
        text(this.health,this.x,this.y-this.width);
    }

        

    end(){
        super.end();
    }

    changeState(state){
        super.changeState(state);
    }

    getDistance(){
        let player = GAME.getObject(GAME.player);
        let distX = 0;
        let distY = 0;

        if(player != undefined){
            distX = this.x - player.x;
            distY = this.y - player.y;
            return(sqrt(distX*distX + distY*distY));
        }else{
            //console.error("Player not found by machine");
        }
    }

    wonder(){
        let time = this.target.wonder.interval + random(this.target.wonder.variation);
        let ship = GAME.getObject(GAME.player);
        if(ship != undefined){
            this.target.x = ship.x;//random(padding,GAME.levelHandler.WORLD_WIDTH-padding);
            this.target.y = ship.y;
        }
        this.changeState(this.states.wonder);
        setTimeout(() => {this.wonder()},time);
    }
}
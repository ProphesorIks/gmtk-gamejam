class GOTest extends GameObject{
    ship;
    constructor(ship){
        super(0,0,GAME.graphicsHandler.assets.sprites.objects.game.bullet, GAME.graphicsHandler.SPRITE_GROUPS.OBJECTS);
        this.ship = ship;
        this.width = 64;
        this.height = 32;
    }

    step(){
        this.x = mouseX;
        this.y = mouseY;
        this.updateSprite();
    }

    bulletStress(n){
        for(let i = 0; i < n; i++){
            GAME.GOHandler.addObjectNoTag(new GOBullet(this.x,this.y,-40));
        }
    }
}